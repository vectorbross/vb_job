(function($, window, document, Drupal) {
	$(window).on('load', function() {
		// get job count and display in nav
		$.get('/vb_job/job_count', function(data) {
			if(data.count) {
				$('.menu--main .jobs-overview').append('<span class="job-count">'+data.count+'</span>');
				setTimeout(function() {
					$('.menu--main .jobs-overview').addClass('jobs-overview--loaded');
				}, 300);
			}
		});
	});
})(jQuery, window, document, Drupal);