<?php

namespace Drupal\vb_job\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring the Job node type.
 */
class VBJobAdminConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_job_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_job.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_job.settings');
    $node_storage = $this->entityTypeManager->getStorage('node');
    // $overview = $config->get('overview');
    // $form['overview'] = [
    //   '#type' => 'entity_autocomplete',
    //   '#title' => $this->t('Overview page'),
    //   '#target_type' => 'node',
    //   '#autocreate' => [
    //     'bundle' => ['page'],
    //   ],
    //   '#required' => TRUE,
    //   '#default_value' => $overview ? $node_storage->load($overview) : NULL,
    // ];
    $form['detail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Jobs link to their node page on the overview.'),
      '#default_value' => $config->get('detail'),
    ];
    $form['menu_counter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add jobs counter to the respective menu item.'),
      '#description' => $this->t('By default, the counter will be added to the menu item that links to the overview page set in our VB overview pages config.<br />If this overview page is not available or you need to add it otherwise, add the <em>jobs-overview</em> class to the menu item through preprocess or menu item attributes module.'),
      '#default_value' => $config->get('menu_counter'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $node_storage = $this->entityTypeManager->getStorage('node');
    //$node = $node_storage->load($values['overview']);
    $this->configFactory->getEditable('vb_job.settings')
      //->set('overview', $values['overview'])
      ->set('detail', $values['detail'])
      ->set('menu_counter', $values['menu_counter'])
      ->save();
    // change seo settings depending on detail value
    $this->configFactory->getEditable('simple_sitemap.bundle_settings.default.node.job')
      ->setData([
        'index' => (bool) $values['detail'],
        'priority' => 0.5,
        'changefreq' => '',
        'include_images' => FALSE,
      ])
      ->save();
    $metatags = $this->configFactory->getEditable('metatag.metatag_defaults.node__job');
    if($metatags->isNew()) {
      $metatags->setData([
        'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        'status' => TRUE,
        'dependencies' => [],
        'id' => 'node__job',
        'label' => 'Content: ' . $this->t('Job'),
        'tags' => [],
      ]);
    }
    $tags = $metatags->get('tags');
    if(!$values['detail']) {
      $tags['robots'] = 'noindex';
    } elseif(isset($tags['robots'])) {
      unset($tags['robots']);
    }
    $metatags->set('tags', $tags);
    $metatags->save();
    // generate pathauto pattern
    // foreach($this->languageManager->getLanguages() as $langcode => $language) {
    //   if($node->hasTranslation($langcode)) {
    //     $this->configFactory->getEditable('pathauto.pattern.job')
    //       ->setData([
    //         'langcode' => $langcode,
    //         'status' => TRUE,
    //         'dependencies' => [
    //           'module' => [
    //             'node',
    //           ],
    //         ],
    //         'id' => 'job',
    //         'label' => $this->t('Job'),
    //         'type' => 'canonical_entities:node',
    //         'pattern' => $node->getTranslation($langcode)->toUrl()->toString() . '/[node:title]',
    //         'selection_criteria' => [
    //           [
    //             'id' => 'node_type',
    //             'bundles' => [
    //               'job' => 'job',
    //             ],
    //             'negate' => FALSE,
    //             'context_mapping' => [
    //               'node' => 'node',
    //             ],
    //           ]
    //         ],
    //         'selection_logic' => 'and',
    //         'weight' => -5,
    //         'relationships' => [],
    //       ])
    //       ->save();
    //   }
    // }
    parent::submitForm($form, $form_state);
  }

}
