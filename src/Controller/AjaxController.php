<?php

namespace Drupal\vb_job\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends ControllerBase {

    /**
     * The entity type manager service
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;


    /**
     * Construct an AjaxController object
     *
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
     *   The entity type manager service
     */
    public function __construct(EntityTypeManagerInterface $entityTypeManager) {
        $this->entityTypeManager = $entityTypeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('entity_type.manager'),
            $container->get('keyvalue')
        );
    }

    /**
     *
     */
    public function getJobCount() {
        $result = $this->entityTypeManager->getStorage('node')->getQuery()->condition('type', 'job')->condition('status', '1')->execute();
        $json['count'] = count($result);
        return new JsonResponse($json);
    }
}
